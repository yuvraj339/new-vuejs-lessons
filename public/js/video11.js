Vue.component('tasks',{

        template:"#task-template",
        //props:['list'],

        data: function () {
            return {
                list: []
            };

        },
        computed: {
            remaining: function () {
                return this.list.filter(this.inProgress).length;
            }
        },
        created:function()
        {
            this.fetchTaskList();
        },

        methods: {
            isCompleted: function (task) {
                return task.completed
            },
            inProgress: function (task) {
                return !this.isCompleted(task);
            },


            deleteTask: function (task) {
                //var resource  = this.$resource('api/tasks/delete/:id')
                var userInfo;
                userInfo = this.taskInfo;

                Vue.http.headers.common['X-CSRF-TOKEN'] = userInfo.token;
                this.$http.post('api/tasks/delete/'+  userInfo.id);
                this.list.$remove(task)
                //resource.delete({id: id}).then(function (tasks) {
                //    // handle success
                //    alert('hello')
                //
                //    this.list.$remove(task)
                //
                //}.bind(this)
                //    , function (tasks) {
                //    // handle error
                //    alert('sorry there is somthing went wrong')
                //});
            },


            clearCompleted: function () {
                this.list = this.list.filter(this.inProgress);
            },
            fetchTaskList: function () {
                var resource  = this.$resource('api/tasks/:id')

                //---Without jquery work with vue resource---- no need jquery.js

                //this.$http.get('api/tasks',function (tasks){
                     //        or

                resource.get(function (tasks){
                        this.list = tasks;
                }.bind(this))



                //------Through jquery----no need vue-resource.js---uncomment jquery.js in lesson11 for working with jquery

                //$.getJSON('api/tasks', function (tasks) {
                //    this.list = tasks;
                //}.bind(this));
            }

        }

    }
)

new Vue(
    {
        el: 'body',
        data: {
            taskInfo :{
                token: '',
                id: '',
            }

        }
    })