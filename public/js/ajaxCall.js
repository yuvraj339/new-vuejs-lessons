Vue.component('tasks', {
    props: ['list'],
    template: '#task-template',

    data: function () {
        return {
            list: []
        };
    },
    computed: {
        remaining: function () {
            return this.list.filter(this.inProgress).length;
        }
    },
    created: function () {
        this.fetchTaskList();
        //this.list = JSON.parse(this.list)
    },
    methods: {
        isCompleted: function (task) {
            return task.completed
        },
        inProgress: function (task) {
            return !this.isCompleted(task);
        },
        deleteTask: function (task) {
            this.list.$remove(task)
        },
        clearCompleted: function () {
            this.list = this.list.filter(this.inProgress);
        },
        fetchTaskList: function () {
            $.getJSON('api/tasks', function (tasks) {
                this.list = tasks;
            }.bind(this));
        }
    }
})
new Vue({
    el: "body",
})