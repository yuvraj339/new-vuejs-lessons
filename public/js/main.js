Vue.component('tasks', {
    props: ['list'],
    template: '#task-template',

    computed: {
        remaining: function () {
            return this.list.filter(this.inProgress).length;
        }
    },
    methods:{
        isCompleted:function(task)
        {
          return task.completed
        },
        inProgress:function(task)
        {
            return !this.isCompleted(task);
        },
        deleteTask:function(task)
        {
            this.list.$remove(task)
        },
        clearCompleted:function()
        {
             this.list = this.list.filter(this.inProgress);
        }
    }
})
new Vue({

    el : ".app",

    data:{
        tasks :[
            {body: 'Go to the School', completed: false},
            {body: 'Go to the Bank', completed: false},
            {body: 'Go to the Market', completed: true}
        ]
    }
})