<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>vue learn</title>
<link href="{{url('/')}}/css/style.css" rel="stylesheet" type="text/css">
<link href="{{url('/')}}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="container-fluid">
   {{--{{ dd(url('') == "http://localhost/vue-learn/public") == true}}--}}
    {{--@if(url('') == "http://localhost/vue-learn/public" )--}}
        <h4><a href="{{url('/')}}">Back To Index</a></h4>

        <hr>
    {{--@endif--}}
    @yield('content')
</div>

<script src="{{url()}}/js/vue.js" type="text/javascript"></script>
<script src="{{url()}}/js/vue-resource.js" type="text/javascript"></script>
@yield('vscript')
</body>

</html>