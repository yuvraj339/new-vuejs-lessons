@extends('layout')
@section('content')
<h1>
    Wellcome to Vue learning point
</h1>
   <h4><a href="veu-lessons/lesson/1">Lesson first : Hello-Databinding</a></h4>
   <h4><a href="veu-lessons/lesson/2">Lesson Secound : V-Show</a></h4>
   <h4><a href="veu-lessons/lesson/3">Lesson Third : Event-Handling</a></h4>
   <h4><a href="veu-lessons/lesson/4">Lesson fourth : A-Peek-Into-Components</a></h4>
   <h4><a href="veu-lessons/lesson/5">Lesson fifth : Computed-Properties</a></h4>
   <h4><a href="veu-lessons/lesson/6">Lesson sixth : Subscription-Plans-Exercise</a></h4>
   <h4><a href="veu-lessons/lesson/7">Lesson seventh : Rendering-and-Working-With-Lists</a></h4>
   <h4><a href="veu-lessons/lesson/8">Lesson eighth : Custom-Components-101</a></h4>
   <h4><a href="veu-lessons/lesson/9">Lesson ninth : Vue-Easy-Tweaks</a></h4>
   <h4><a href="veu-lessons/lesson/10">Lesson tenth : Vue-Laravel-and-AJAX-Requests</a></h4>
   <h4><a href="veu-lessons/lesson/11">Lesson eleventh  : Vue-Resource</a></h4>
   <h4><a href="veu-lessons/lesson/12">Lesson twelfth  : Component-Exercise</a></h4>
@stop
