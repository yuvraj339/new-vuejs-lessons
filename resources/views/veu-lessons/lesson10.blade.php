@extends('layout')
@section('content')

    <div class="app">
        {{--<tasks :list="{{ $tasks }}"></tasks>--}}

        <tasks></tasks>
    </div>

    <template id="task-template">
        <h2> My Tasks from jQuery
            {{--//v-show only remaining task if zero then not show anything--}}
            <span v-show="remaining">(@{{ remaining }})</span>
        </h2>
        <ul v-show="list.length">
            <li
                    :class="{ 'clsCompleted': task.completed }"
                    v-for="task in list"
                    @click="task.completed = ! task.completed"
            >
            @{{ task.body }}
            <span class="redText" @click="deleteTask(task)"> X </span>
            </li>
        </ul>
        <p v-else>No list yet</p>
        <button @click="clearCompleted">Clear Completed</button>
    </template>

@stop
@section('vscript')
    <script src="{{url()}}/js/jquery.min.js" type="text/javascript"></script>

    <script src="{{url()}}/js/ajaxCall.js" type="text/javascript"></script>
@stop