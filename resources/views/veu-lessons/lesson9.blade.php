@extends('layout')
@section('content')
    <div class="app">
        <tasks :list="tasks"></tasks>

        <tasks :list="[{body: 'done lesson eight',completed:false}]"></tasks>
    <pre>
        <h2>Json Rresponce</h2>
        @{{$data | json}}
    </pre>
    </div>

    <template id="task-template">
        <h2> My Tasks
            {{--//v-show only remaining task if zero then not show anything--}}
            <span v-show="remaining">(@{{ remaining }})</span>
        </h2>
        <ul v-show="list.length">
            <li
                    :class="{ 'clsCompleted': task.completed }"
                    v-for="task in list"
            @click="task.completed = ! task.completed"
            >
            @{{ task.body }}
            <span class="redText" @click="deleteTask(task)"> X </span>
            </li>
        </ul>
        <p v-else>No list yet</p>
        <button @click="clearCompleted">Clear Completed</button>
    </template>

@stop
@section('vscript')
    <script src="{{url()}}/js/main.js" type="text/javascript"></script>
@stop