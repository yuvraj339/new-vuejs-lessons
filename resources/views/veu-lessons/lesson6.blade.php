@extends('layout')
@section('content')

    <div id="app">
        <pre>
            @{{ $data | json }}
        </pre>
        <div v-for="plan in plans">
            <plan :plan="plan" :active.sync="active"></plan>
        </div>
    </div>
    <template id="plan-template">
        <div>
            <span class="width100px">@{{ plan.name }}</span>
            <span class="width100px">@{{ plan.price }}/Month</span>

            <input type="button" name="isUpgrade"
                   v-show="plan.name !== active.name"

                   value="@{{ isUpgrade ?  'Upgrade' : Downgrade }}"
                   @click="setActivePlan"
            >
            <span v-else>
                    Selected
            </span>
        </div>
    </template>
@stop
@section('vscript')
    <script>
        Vue.component('plan', {
            template: "#plan-template",
            props: ['plan', 'active'],
            computed: {
                isUpgrade: function () {
                    return this.plan.price > this.active.price
                }
            },

            methods: {
                setActivePlan: function () {
                    this.active = this.plan;
                }
            }
        });

        new Vue({
            el: '#app',

            data: {
                plans: [
                    {name: 'Enterprise', price: 100},
                    {name: 'pro', price: 50},
                    {name: 'personal', price: 20},
                    {name: 'free', price: 0}
                ],
                active: {
                    "name": 'Downgrade',
                    "price": 0
                }
            },

        });
    </script>
@stop