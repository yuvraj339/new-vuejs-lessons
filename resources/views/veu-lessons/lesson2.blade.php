@extends('layout')
@section('content')

<div class="app">
    <form>
        <span class="error" v-show="!message">
            please enter some text first
        </span>
            <br>
        <textarea v-model="message">
                hello
        </textarea>
        <br>
        <button type="submit" v-show="message">
            Send
        </button>
    </form>
    <pre>
        <h2>Json Rresponce</h2>
        @{{$data | json}}
    </pre>
</div>

@stop
@section('vscript')
    <script>
        new Vue({
            el: '.app',
            data: {
                message: '',
            }
        });
    </script>
@stop