@extends('layout')
@section('content')
    <div class="app">
    <tasks :list="tasks"></tasks>

    <tasks :list="[{body: 'done lesson eight',completed:false}]"></tasks>
    <pre>
        <h2>Json Rresponce</h2>
        @{{$data | json}}
    </pre>
    </div>
    <template id="task-template">
        <h3>
            <ul>
                <li
                        :class="{ 'clsCompleted': task.completed }"
                        v-for="task in list"
                @click="task.completed = ! task.completed"
                >
                @{{ task.body }}
                </li>
            </ul>
        </h3>
    </template>

@stop
@section('vscript')
    <script src="{{url()}}/js/main.js" type="text/javascript"></script>
@stop