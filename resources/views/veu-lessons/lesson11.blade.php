@extends('layout')
@section('content')

    <div class="container">
        {{--//no need to pass any argument--}}
        <tasks></tasks>

    </div>

    <template id="task-template">
        <h3> My Tasks from Vue js
            {{--//v-show only remaining task if zero then not show anything--}}
            <span v-show="remaining">(@{{ remaining }}) <button class="pull-right" @click="clearCompleted">Delete Selected</button></span>
        </h3>
        <form method="post">
            <input type="hidden" v-model="taskInfo.token" name="_token" value="{{ csrf_token() }}">

            <ul v-show="list.length">

                <li v-for="task in list">

                <span class="glyphicon-ban-circle bigBox"
                      :class="{ 'clsCompleted': task.completed }"
                    @click="task.completed = ! task.completed"></span>


                    <span>@{{ task.body }}</span>
                    <input type="hidden" v-model="taskInfo.id" name="id" value="@{{task.id}}">

                    <span class="redText" @click="deleteTask(task)"> X </span>

                </li>
            </ul>
            <p v-else>No list yet</p>
            <button @click="clearCompleted">Delete Selected</button>
        </form>
    </template>

@stop
@section('vscript')
    {{--<script src="{{url()}}/js/jquery.min.js" type="text/javascript"></script>--}}
    <script src="{{url()}}/js/video11.js" type="text/javascript"></script>
@stop