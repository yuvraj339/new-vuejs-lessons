@extends('layout')
@section('content')
    <h1>
        {{--//no need to pass any argument--}}
        Different Alert Types

    </h1>
    <hr>
    <alert >
        <strong>Default !</strong> your account has been updated successfully
    </alert>
    <alert type="general">
        <strong>General !</strong> your account has been updated successfully
    </alert>

    <alert type="error">
        <strong>Error !</strong> your account has been updated successfully
    </alert>

    <alert type="success">
        <strong>Success !</strong> your account has not been updated successfully
    </alert>

    <alert type="warning">
        <strong>Warning !</strong> your account has not been updated successfully
    </alert>


    <template id="alert-template">
        <div :class="alertClasses" v-show="show">
            <slot></slot>

            <span class="Alert__close" @click="show = false">X</span>
        </div>
    </template>

@stop
@section('vscript')
    {{--<script src="{{url()}}/js/jquery.min.js" type="text/javascript"></script>--}}
    <script src="{{url()}}/js/video12.js" type="text/javascript"></script>
@stop