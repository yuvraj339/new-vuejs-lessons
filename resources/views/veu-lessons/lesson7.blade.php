@extends('layout')
@section('content')
    <div class="app">
        <h1>
            <ul>
                <li
                        :class="{ 'clsCompleted': task.completed }"
                        v-for="task in tasks"
                        @click="task.completed = ! task.completed"
                >
                    @{{ task.body }}
                </li>
            </ul>
        </h1>
<pre>
    <h2>Json Rresponce</h2>
    @{{$data | json}}
</pre>
    </div>

@stop
@section('vscript')
    <script src="{{url()}}/js/main.js" type="text/javascript"></script>
@stop