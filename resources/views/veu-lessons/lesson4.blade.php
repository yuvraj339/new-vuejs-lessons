@extends('layout')
@section('content')

    <div class="app">
        <counter subject="Likes"></counter>
        <counter subject="Dislike"></counter>
    </div>
    <template id="likeDislikeCounter">
        <h2>@{{ subject }}</h2>
        <button @click="count += 1"> @{{ count }} </button>
    </template>
@stop
@section('vscript')
    <script>
        Vue.component('counter', {
            template: '#likeDislikeCounter',
            props: ['subject'],
            data: function () {
                return {count: 0};
            }
        });
        new Vue({
            el: '.app',
        });
    </script>
@stop