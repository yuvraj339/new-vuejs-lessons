@extends('layout')
@section('content')

    <div class="app">
        <h1>
            Skill : @{{ skill }}
        </h1>
        <input v-model="points" placeholder="Please enter a number and change abow skill" size="100px">
        <pre>
            <h2>Json Rresponce</h2>
            @{{$data | json}}
        </pre>
    </div>
    <hr>

    <h1>Example 2</h1>
    <input v-model="first" placeholder="Please enter first name" size="50px">
    <input v-model="last" placeholder="Please enter last name" size="50px">

    <h1>
        @{{ fullname }}
    </h1>

@stop
@section('vscript')
    <script>
        new Vue({
            el: 'body',
            data: {
                points: '',
                first : 'Yuvraj',
                last : 'shekhawat',
            },
            // computed function compute and work on it
            computed: {
                skill: function () {
                    if (this.points <= 100) {
                        return "Beginer"
                    }
                    return "Advance"
                },
                fullname : function(){
                    return this.first + ' ' + this.last;
                }
            }
        });
    </script>
@stop