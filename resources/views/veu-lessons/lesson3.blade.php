@extends('layout')
@section('content')

    <div class="app">

        <button @click="updateCount">
            Increment Counter : @{{ count }}
        </button>
        <form action="lesson1.blade.php" v-on:submit.prevent="handelIt">

            <button type="submit">
                Handel
            </button>
        </form>
    <pre>
        <h2>Json Rresponce</h2>
        @{{$data | json}}
    </pre>
    </div>

@stop
@section('vscript')
    <script>
        new Vue({
            el: '.app',
            data: {
                count: 0,
            },
            methods: {

                updateCount: function () {
                    this.count += 1
                },
                handelIt: function () {
                    alert('handel')
                }
            }
        });
    </script>
@stop