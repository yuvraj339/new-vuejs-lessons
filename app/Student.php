<?php

namespace App;

use App\Group;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
   public function groups()
   {
      return $this->belongsToMany(Group::class)->withTimestamps();
//      return $this->hasMany('App\Group');
   }
}
