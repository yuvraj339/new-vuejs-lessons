<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Task;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function showTask()
    {
        return Task::latest()->get();

    }
    public function deleteTask($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
         return Task::latest()->get();
    }

    public function vueLesson($no)
    {
        if($no >= 10)
        {
            $tasks = Task::latest()->get();

            return view('veu-lessons.lesson'.$no, compact('tasks'));
        }
        return view('veu-lessons.lesson'.$no);
    }
}
