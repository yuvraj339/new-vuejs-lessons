<?php

use App\User;
use App\Events\UserWasBanned;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//directly call from javascript for video11
Route::get('veu-lessons/lesson/api/tasks','TaskController@showTask');

Route::post('veu-lessons/lesson/api/tasks/delete/{id}','TaskController@deleteTask');

Route::get('veu-lessons/lesson/{no}','TaskController@vueLesson');

Route::get('events',function(){

    $user = new User();
    event(new UserWasBanned($user));

});
